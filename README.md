#AUTOMATOS FINITOS DETERMINISTICOS
##Implementações de AFDs para análise léxica

Este programa oferece três abordagens de implementação de automatos finitos determinísticos que podem ser utilizados para análise léxica de compiladores. As saídas do progama, entretanto, não diferem entre uma abordagem e outra. Em cada implementação há dois autômatos: um que lê um identificador da linguagem tiny e outro que lê um comentário na linguagem c.

###Abordagens
As três abordagens implementadas diferem apenas em código, pois entrada e saída são idênticos, sendo a primeira abordagem a mais simples e menos robusta, sem definição explícita de estados e baseada somente no encadeamento de estrutudas de condição e repetição, enquanto a segunda se aproxima mais de uma máquina de estados finitos, já com definição de estados. A terceira é a mais robusta e já tem a estrutura estado/transição. Durante a execução do programa, o usuário pode escolher a abordagem desejada.

###Instalação

O programa está escrito na linguagem C. Junto com os códigos fontes, também é fornecido um arquivo em lotes (install.bat) que faz a compilação do programa, mas, se o usuário preferir, ou se não estiver na plataforma Windows, há também a possibilidade de instalar manualmente, como segue:
	```gcc *.c -o _nomedoexecutavel_```
ou
	```gcc automato_1.c automato_2.c automato_3.c main.c -o _nomedoexecutavel_```

###Utilização

O programa recebe parâmetros via linha de comando, portanto é necessário que seja executado por um terminal/shell/interpretador de comandos (Windows Powershell, Prompt de Comando, Bash). Ainda, as entradas são feitas por meio de (Um ou diversos) arquivos cujos nomes são passados por parâmetros também. Para executar o programa, o usuário deve seguir a seguinte sintaxe:

Windows:
	```nomedoexecutavel abordagem nome_arquivo1 automato1 ...```

Sistemas baseados em UNIX:
	```./nomedoexecutavel abordagem nome_arquivo1 automato1 ...```

sendo as abordagens assosciadas aos numeros correspondentes (1, 2, 3)
e automatos 1 (ID em tiny) ou 2 (Comentario em C)

ex:
	```afd.exe 1 comentario.txt 2 identificador.txt 1 identificador2.txt 1 comentario2.txt 2```

a saída produzida será dada na própria tela do terminal, por exemplo, em caso de nenhuma das entradas serem aceitas:
	```
	comentario.txt:
	Erro
	identificador.txt
	Erro
	identificador2.txt
	Erro
	comentario2.txt
	Erro
	```









