#include <stdio.h>
#include <stdlib.h>
#include "utils.h"
#include "automato_1.h"

int id(FILE *f)
{	
	char c;
	int i = 0;

	c = fgetc(f);
	/* estado 1 */
	if(c != EOF && type(c) == LETRA)
	{
		i++; c = fgetc(f);
		/*estado 2*/
		while(c != EOF && type(c) == LETRA || type(c) == DIGITO)
		{
			i++; c = fgetc(f);
		}

		return i;
	}
	else
	{
		return 0;
	}
}

int commentC(FILE *f)
{
	char c;
	int fim, i = 0;

	c = fgetc(f);
	if(c != EOF && c == '/')
	{
		i++; c = fgetc(f);
		if(c != EOF && c == '*')
		{
			i++; c = fgetc(f);
			fim = 0;
			while(!fim){
				while(c != EOF && c != '*')
				{
					i++; c = fgetc(f);
				}
				i++; c = fgetc(f);
				while(c != EOF && c == '*')
				{
					i++; c = fgetc(f);
				}
				if(c != EOF && c == '/')
				{
					fim = 1;
				}else if(c == EOF)
					return 0;
				i++; fgetc(f);
			}
			return i;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}
}

int main_1(int argc, char **argv)
{
	FILE *f;
	int i, j;
	int ret;


	for(i = 2, j = 3; j < argc; i+=2, j+=2)
	{
		printf("Arquivo: %s\n",argv[i]);
		f = fopen(argv[i], "r");
		switch(argv[j][0])
		{
		case '1':
			ret = id(f);
			break;
		case '2':
			ret = commentC(f);
			break;
		default:
			ret = -1;
			break;
		}

		if(ret > 0)
		{
			char *str = lexema(f, ret);
			printf("Aceito:\n%s\n", str);	
			free(str);	
		}
		else		
			printf("Erro\n");		
		
		fclose(f);
	}

	return ret < 0 ? ret : 0;
}
