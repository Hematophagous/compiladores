#include <stdio.h>
#include <stdlib.h>
#include "automato_3.h"
#include "utils.h"

estado *inicializaEstado(int aceita)
{
    estado *v = NULL;

	v = (estado *)malloc(sizeof(estado));

	if(v != NULL)
	{
		v -> aceita = aceita;

		v -> trans = NULL;

		v -> next = NULL;

	}

	return v;
}

leitura *inicializaTrans(char c, estado *q1, estado *q2)
{
    leitura *a = NULL;

	a = (leitura *)malloc(sizeof(leitura));

	if(a != NULL)
	{
		a -> c = c;

		a -> org = q1;

		a -> dest = q2;

		a -> next = NULL;
	}

	return a;
}


int adicionarEstado(automato *g, estado *v)
{
    estado *prox, *ante;

    if(g == NULL || v == NULL)
		return 0;

	if(g -> inicial == NULL)
	{
		g -> inicial = v;

		return 1;
	}

    ante = g -> inicial;
	prox = ante -> next;

    while(prox != NULL)
    {
        ante = prox;
        prox = prox -> next;
    }

    ante -> next = v;

    return 1;
}

void ligarEstado(estado *origem, estado *destino, char c)
{
    leitura *ante, *prox;

    if(origem == NULL)
        return ;

    if(origem -> trans == NULL)
    {
        origem -> trans = (void *)inicializaTrans(c, origem, destino);
        return ;
    }

    ante = (leitura *)(origem -> trans);
    prox = (ante -> next);

    while(prox != NULL)
    {
    	// printf(".");
        ante = prox;
        prox = prox -> next;
    }

    ante -> next = inicializaTrans(c, origem, destino);
}

automato *automatoComentario()
{
    automato *comment = NULL;
    estado *q0, *q1, *q2, *q3, *q4;

    comment = (automato *)malloc(sizeof(automato));
    if(comment != NULL){
	    comment -> inicial = NULL;
	    q0 = inicializaEstado(0);
	    q1 = inicializaEstado(0);
	    q2 = inicializaEstado(0);
	    q3 = inicializaEstado(0);	   
	    q4 = inicializaEstado(1);
	   

	    adicionarEstado(comment, q0);
	    adicionarEstado(comment, q1);
	    adicionarEstado(comment, q2);
	    adicionarEstado(comment, q3);
	    adicionarEstado(comment, q4);

	    ligarEstado(q0, q1, '/');
	    ligarEstado(q1, q2, '*');
	    ligarEstado(q2, q2, '/');
	    ligarEstado(q2, q3, '*');
	    ligarEstado(q2, q2, ' ');
	    ligarEstado(q3, q4, '/');
	    ligarEstado(q3, q3, '*');
	    ligarEstado(q3, q2, ' ');

	}

    return comment;
}

estado *encontraEstado(estado *q0, char c)
{
	leitura *t = (leitura *)(q0 -> trans);

	c = (c == '/' ? '/' : (c == '*' ? '*' : ' '));

	while(t != NULL)
	{
		if(t -> c == c){
			return t -> dest;
		}
		t = (leitura *)(t -> next);
	}
	
	return NULL;
}

void destroyAutomata(automato *g)
{
  estado *aux, *vaux;
  leitura *t, *taux;

  if(g == NULL)
    return ;

  aux = g -> inicial;

  while(aux != NULL)
  {
      vaux = aux -> next;
      t = (leitura *)(aux -> trans);
      while(t != NULL)
      {
        taux = t -> next;
        free(t);
        t = taux;
      }
      free(aux);
      aux = vaux;

  }

}
int commentC3(FILE *f)
{
	automato *comentario;
	char c;
	estado *state, *newState = NULL;
	int i = 0;

	comentario = automatoComentario();
	state = comentario -> inicial;
	c = fgetc(f);
	while(c != EOF && state != NULL && !(state -> aceita))
	{
		newState = encontraEstado(state, c);
		if(newState != NULL)
			i++; c = fgetc(f);

		state = newState;
	}
	if(state == NULL || !(state -> aceita)){
		i = 0;
	}

  destroyAutomata(comentario);



  free(comentario);

  return i;
}


int main_3(int argc, char **argv)
{
	FILE *f;
	int i, j;
	int ret;

	for(i = 2, j = 3; j < argc; i+=2, j+=2)
	{
		f = fopen(argv[i], "r");
		switch(argv[j][0])
		{
		case '1':
		case '2':
      // printf("file: %s\n", argv[i]);
			ret = commentC3(f);
			break;
		default:
			ret = -1;
			break;
		}

		if(ret > 0)
		{
			char *str = lexema(f, ret);
			printf("Aceito:\n%s\n", str);
			free(str);
		}
		else
			printf("Erro\n");

		fclose(f);
	}

	return ret < 0 ? ret : 0;
}

/*
int main_3(int argc, char **argv)
{
	FILE *f = fopen("str1", "r");

	int ret = commentC3(f);
	printf("\n%d", ret);
	char *str = lexema(f, ret);

	printf("%s\n", str);

	fclose(f);
	free(str);

	return 0;
}

/*
int main(int argc, char **argv)
{

	return 0;
}

*/
