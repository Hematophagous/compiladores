/**
	* @file 	automato_3.h
	* @brief 	arquivo contendo as definicoes das funcoes e estruturas de dados utilizadas na terceira abordagem de implementacao de um automato
	* @author 	Gabriel Bras & Elisomar Abreu
	* @date		16 de Setembro de 2019
*/

typedef struct _state{
    int aceita;
    struct _state *next;
    void *trans;
}estado;

typedef struct _leitura{
    estado *org;
    char c;
    estado *dest;
    struct _leitura *next;
}leitura;

typedef struct _automato{
    estado *inicial;
}automato;

/**
	* @fn 		int commentC3(FILE *f)
	* @brief	funcao com o automato que aceita (ou nao) uma cadeia de caracteres como comentario da linguagem C
	* @param	f ponteiro para o arquivo contendo a sequencia de caracteres
	* @return 	tamanho da sequencia de caracteres aceita como comentario, ou
	* @return 	0, se não for aceita
*/
int commentC3(FILE *f);

/**
	* @fn 		estado *inicializaEstado();
	* @brief	funcao que inicializa um estado (define se é final ou não e armazena transições) do automato
	* @return 	estado, se alocado, ou
	* @return 	NULL, caso contrário
*/
estado *inicializaEstado();

/**
	* @fn 		leitura *inicializaTrans();
	* @brief	funcao que inicializa uma transicao (define se o rótulo e o destino) do automato
	* @param	c rotulo da transicao
	* @param	q1 estado de origem da transicao
	* @param	q2 estado de destino da transicao
	* @return 	transicao, se alocada, ou
	* @return 	NULL, caso contrário
*/
leitura *inicializaTrans(char c, estado *q1, estado *q2);

/**
	* @fn 		int adicionarEstado(automato *g, estado *v);
	* @brief	funcao que adiciona um estado ao automato
	* @param	g automato ao qual adicionar estado
	* @param	v estado a ser adicionado
	* @return 	1, se adicionado, ou
	* @return 	0, caso contrário
*/
int adicionarEstado(automato *g, estado *v);

/**
	* @fn 		void ligarEstado(estado *origem, estado *destino, char c);
	* @brief	funcao que adiciona uma transição à lista de transiçoes de um estado
	* @param	origem estado ao qual a transicao será adicionada
	* @param	destino estado de destino
	* @param	c, rótulo da transicao a ser adicionada
	* @return 
*/
void ligarEstado(estado *origem, estado *destino, char c);

/**
	* @fn 		estado *encontraEstado(estado *q0, char c);
	* @brief	dado um rótulo e um estado, essa funcao procura o estado de origem da transicao com aquele rótulo, se ela existir
	* @param	q0, estado de origem
	* @param	c, rótulo da transicao a ser verificada
	* @return 	estado de destino da transicao, se ela existir, ou
	* @return 	NULL, caso contrario
*/
estado *encontraEstado(estado *q0, char c);

/**
	* @fn 		void destroyAutomata(automato *g);
	* @brief	desaloca toda a memória alocada na construção do automato
	* @param	g, automato a ser destruido
	* @return 
*/
void destroyAutomata(automato *g);

/**
	* @fn 		int main_3(int argc, char **argv);
	* @brief	funcao principal do automato
	* @param	argc, quantidade de parametros passados
	* @param	argv, parametros
	* @return 
*/
int main_3(int argc, char **argv);
