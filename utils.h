/**
	* @file 	utils.h
	* @brief 	arquivo contendo definicoes de funcoes auxiliares utilizadas nos automatos
	* @author	Gabriel Bras
	* @date 	14 de Setembro de 2019
*/

#ifndef UTILS_H
#define UTILS_H

#define OUTRO  -1
#define DIGITO  0
#define LETRA   1

/**
	* @fn 		int type(char c)
	* @brief	funcao que identifica o tipo do caractere passado (LETRA, DIGITO, OUTRO)
	* @param 	c caractere a ser classificado
	* @return	LETRA, DIGITO OU OUTRO, de acordo com o tipo identificado
*/
int type(char c);

/**
	* @fn 		char *lexema(FILE *f, int n)
	* @brief	funcao que copia uma sequencia de caracteres de um arquivo para uma string
	* @param 	f ponteiro para o arquivo de origem
	* @param 	n tamanho da sequencia de caracteres
	* @return	string contendo a sequencia de caracteres lida
*/
char *lexema(FILE *f, int n);

#endif //UTILS_H