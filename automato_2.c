#include <stdio.h>
#include <stdlib.h>
#include "utils.h"
#include "automato_2.h"

int id2(FILE *f)
{
	char c;
	int i = 0, state;

	state = 1;
	c = fgetc(f);
	while((state == 1 || state == 2 || state == 3))
	{
		switch(state) 
		{
		case 1:
			switch(type(c))
			{
				case LETRA:
					i++; c = fgetc(f);
					state = 2;
					break;
				default:
					state = -1;
					break;
			}
			break;
		case 2:
			switch(type(c))
			{
				case LETRA:
				case DIGITO:
					i++; c = fgetc(f);
					break;
				default:
					// printf("\n%d",type(c));
					state = 3;
					break;
			}
			break;
		case 3:
			return i;
		default:
			return 0;
		}
	}
	return 0;
}

int commentC2(FILE *f)
{
	char c;
	int i = 0, state;

	state = 1;
	c = fgetc(f);
	while(c != EOF && (state > 0 && state <= 5))
	{
		
		switch(state)
		{
		case 1:
			switch(c)
			{
			case '/':
				i++; c = fgetc(f);
				state = 2;
				break;
			default:
				state = -1;
			}
			break;
		case 2:

			switch(c)
			{
			case '*':
				i++; c = fgetc(f);
				state = 3;
				break;
			default:
				state = -1;
			}
			break;
		case 3:
			switch(c)
			{
			case '*':
				i++; c = fgetc(f);
				state = 4;
				break;
			default:
				i++; c = fgetc(f);
			}
			break;
		case 4:
			switch(c)
			{
			case '/':
			{
				i++;
				state = 5;
				break;
			}
			case '*':
				i++; c = fgetc(f);
				break;
			default:
				i++; c = fgetc(f);
				state = 3;
			}
			break;
		case 5:
			return i;
			break;
		case -1:
			return 0;
		}
	}

	return 0;
}

int main_2(int argc, char **argv)
{
	FILE *f;
	int i, j;
	int ret;

	for(i = 2, j = 3; j < argc; i+=2, j+=2)
	{
		f = fopen(argv[i], "r");
		switch(argv[j][0])
		{
		case '1':
			ret = id2(f);
			break;
		case '2':
			ret = commentC2(f);
			break;
		default:
			ret = -1;
			break;
		}

		if(ret > 0)
		{
			char *str = lexema(f, ret);
			printf("Aceito:\n%s\n", str);
			free(str);
		}
		else
			printf("Erro\n");

		fclose(f);
	}

	return ret < 0 ? ret : 0;
}
