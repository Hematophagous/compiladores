#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

int type(char c)
{
	if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
		return LETRA;
	else if(c >= '0' && c <= '9')
		return DIGITO;
	else
		return OUTRO;
}

char *lexema(FILE *f, int n)
{
	char *str;
	int i;
	fseek(f, 0, SEEK_SET);
	str = (char *)malloc((n+1)*sizeof(char));
	for(i = 0; i < n; i++)
		str[i] = fgetc(f);
	str[i] = '\0';
	return str;
}