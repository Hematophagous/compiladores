#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "automato_1.h"
#include "automato_2.h"
#include "automato_3.h"


int main(int argc, char **argv)
{
	int ret;
	char *c = argv[1];

	switch(c[0])
	{
	case '1':
		ret = main_1(argc, argv);
		break;
	case '2':
		ret = main_2(argc, argv);
		break;
	case '3':
			ret = main_3(argc, argv);
			break;
	}

	return ret;
}
