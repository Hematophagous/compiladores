/**
	* @file 	automato_1.h
	* @brief 	arquivo contendo definicoes das funções utilizadas na primeira implementacao da disciplina Compiladores
	* @author	Gabriel Bras
	* @date 	14 de Setembro de 2019
*/

#ifndef AUTOMATO_1
#define AUTOMATO_1

/**
	* @fn 		int id(FILE *f)
	* @brief	funcao que aceita(ou nao) uma sequencia de caracteres como um identificador na linguagem tiny
	* @param 	f arquivo contendo a sequencia de caracteres
	* @return	tamanho da sequencia de caracteres do identificador, se aceitar, ou
	* @return	0, caso contrario
*/
int id(FILE *f);

/**
	* @fn 		int commentC(FILE *f)
	* @brief	funcao que aceita(ou nao) uma sequencia de caracteres como um comentario na linguagem C
	* @param 	f arquivo contendo a sequencia de caracteres
	* @return	tamanho da sequencia de caracteres do comentario, se aceitar, ou
	* @return	0, caso contrario
*/
int commentC(FILE *f);

int main_1(int argc, char **argv);

#endif //AUTOMATO_1